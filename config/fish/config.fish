
set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
set -q XDG_CACHE_HOME; or set XDG_CACHE_HOME ~/.cache
set -q XDG_DATA_HOME; or set XDG_DATA_HOME ~/.local/share

if not functions -q fisher
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

set -gx USESUDO sudo
set -gx FORTIPKG $HOME/fortipkg

set pure_symbol_prompt "𝝺"
set pure_user_host_location 1

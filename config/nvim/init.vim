call plug#begin('~/.local/share/nvim/site/plugged')

Plug 'liuchengxu/vim-better-default'
Plug 'junegunn/seoul256.vim'
Plug 'bling/vim-airline'

Plug 'junegunn/fzf', { 'do': './install --bin' }
Plug 'junegunn/fzf.vim'
Plug 'jesseleite/vim-agriculture'

Plug 'vim-scripts/gtags.vim'
Plug 'jsfaint/gen_tags.vim'

Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-commentary', { 'on': '<Plug>Commentary' }
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': { -> coc#util#install() }}

Plug 'vim-scripts/vcscommand.vim'
Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'
Plug 'vivien/vim-linux-coding-style'
Plug 'Yggdroot/indentLine', { 'on': 'IndentLinesEnable' }

call plug#end()

set grepprg=rg\ --vimgrep
set updatetime=300
set shortmess+=c
set cscopequickfix=s-,c-,d-,i-,t-,e-,g-,f-,a-
set mouse=a

colo seoul256
let mapleader      = ' '
let maplocalleader = ' '
let g:agriculture#disable_smart_quoting = 1
let g:VCSCommandSplit = 'vertical'
let g:vim_better_default_backup_on = 1
let g:vim_better_default_persistent_undo = 1
let g:vim_better_default_enable_folding = 0
let g:indentLine_setColors = 0
let g:indentLine_setConceal = 0
let g:gen_tags#ctags_opts = '--skip-unreadable --skip-symlink'

map               gc               <Plug>Commentary
xmap              <Enter>          <Plug>(EasyAlign)
nmap              <Leader>/        <Plug>RgRawSearch
nnoremap <silent> <Leader><Leader> :Files<CR>
nnoremap <silent> <Leader><Enter>  :Buffers<CR>

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

autocmd QuickFixCmdPost * botright copen

runtime! plugin/default.vim
set list listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace
nnoremap <CR> <CR>


# vim: set filetype=python :

$PATH.insert(0, "/home/linuxbrew/.linuxbrew/bin")

$XONSH_HISTORY_BACKEND = 'sqlite'
$USESUDO = 'sudo'

aliases.update({
    '..':  'cd ..',
    'll':  'exa -l',
    'la':  'exa -a',
    'lla': 'exa -la',
    'vi':  'nvim',
    'vim': 'nvim'
})
